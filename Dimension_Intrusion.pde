/**
 * Dimension Intrusion.
 * It draws something fearful thing with the Vector Field.
 *
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.03.22
 */

void setup() {

  size(720, 720);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noStroke();

}

void draw() {

  int   frmMax  = 24 * 6; // for 24fps x 6sec animation
  float baseHue = random(360.0);
  float baseBri = 70.0;
  float baseSiz = min(width, height) * 0.05;

  // shape parameters
  float plotDiv = 0.0004;
  float paramA  = random(1.0, 1.5);
  float paramB  = random(3.5, 4.5);
  float initDiv = random(0.04, 0.05);

  for (int frmCnt = 0; frmCnt < frmMax; ++frmCnt) {

    background((baseHue + 90.0) % 360.0, 40.0, 40.0, 100);
    pushMatrix();
    translate(width * 0.5, -height * 0.4);  
    rotate(HALF_PI * 0.5);

    // draw vector field with custom noise
    int   plotMax = floor(map(frmCnt, 0, frmMax, 20.0, 850.0));
    for (float xInit = 0.2; xInit <= 0.8; xInit += initDiv) {
      for (float yInit = 0.2; yInit <= 0.8; yInit += initDiv) {
        float xPoint = xInit;
        float yPoint = yInit;
        for (int plotCnt = 0; plotCnt < plotMax; ++plotCnt) {

          float plotRatio = map(plotCnt, 0, plotMax, 0.0, 1.0);
          float eHue = baseHue + plotRatio * 30.0 + floor(((xInit * yInit) * 10000.0) % 4.0) * 30.0;;
          float eSat = map(sin(PI * plotRatio), 0.0, 1.0, 60.0, 40.0);
          float eBri = baseBri * (1.5 - sin(PI * plotRatio));
          float eSiz = baseSiz * sin(PI * plotRatio);

          float xPrev = xPoint;
          float yPrev = yPoint;
          xPoint += plotDiv * pow(cos(TWO_PI * customNoise(xPrev * paramA, yPrev * paramB)), 2);
          yPoint += plotDiv * pow(cos(TWO_PI * customNoise(yPrev * paramA, xPrev * paramB)), 2);

          fill(eHue % 360.0, eSat, eBri, 30.0);
          ellipse(xPoint * width, yPoint * height, eSiz, eSiz);

        }
      }
    }
    popMatrix();
      
    casing();
    saveFrame("frames/01." + String.format("%04d", frmCnt) + ".png");

  }

  // for Twitter thumb nail
  saveFrame("frames/00.0000.png");
  for (int i = 0; i < 24 * 2; i++) {
    saveFrame("frames/02." + String.format("%04d", i) + ".png");
  }
  exit();

}

/**
 * casing : draw fancy casing
 */
private void casing() {
  blendMode(BLEND);
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(58.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
  noStroke();
  noFill();
}

/**
 * customNoise : returns almost random but interesting value
 */
float customNoise(float _x, float _y) {
  return pow(sin(_x), 3) * cos(pow(_y, 2));
}
